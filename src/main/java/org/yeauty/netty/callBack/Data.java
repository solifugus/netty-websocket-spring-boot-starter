package org.yeauty.netty.callBack;


/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty
 * @Date 2024/2/24 1:37
 */

public class Data {
    private int n;
    private int m;
    public Data(int n, int m){
        this.n = n;
        this.m = m;
    }

    @Override
    public String toString() {
        int r = n/m;
        return "r = " + r;
    }
}
