package org.yeauty.netty.callBack;

/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty
 * @Date 2024/2/24 1:36
 */
public interface Fetcher {

    void fetchData(FetcherCallback callback);
}
