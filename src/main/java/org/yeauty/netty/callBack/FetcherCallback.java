package org.yeauty.netty.callBack;

/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty
 * @Date 2024/2/24 1:37
 */
public interface FetcherCallback {
    void onData (Data data) throws Exception;
    void onError(Throwable throwable);
}
