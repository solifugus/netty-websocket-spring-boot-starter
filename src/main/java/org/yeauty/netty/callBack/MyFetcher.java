package org.yeauty.netty.callBack;

/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty
 * @Date 2024/2/24 1:36
 */
public class MyFetcher implements Fetcher{

    final Data data;

    public MyFetcher (Data data){
        this.data = data;
    }

    @Override
    public void fetchData(FetcherCallback callback) {
        try {
            callback.onData(data);
        } catch (Exception e) {
            callback.onError(e);
        }
    }
}
