package org.yeauty.netty.callBack;

/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty
 * @Date 2024/2/24 1:34
 */
public class Worker {

    public void doWork(){
        Fetcher fetcher = new MyFetcher(new Data(1,0));
        fetcher.fetchData(new FetcherCallback() {
            @Override
            public void onData(Data data) throws Exception {
                System.out.println("data:" + data);
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("error:" + throwable.getMessage());
            }
        });
    }

    public static void main(String[] args) {
        Worker w = new Worker();
        w.doWork();
    }
}
