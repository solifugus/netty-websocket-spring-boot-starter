package org.yeauty.netty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;

/**
 * 1. 和服务端建立连接
 * 2. 写入数据，发送给服务端
 * 3. 等待从服务端接收未被修改的数据
 * 4. 关闭连接
 * @author huangzhiwei
 * @Classname org.yeauty.netty.client
 * @Date 2024/2/24 2:58
 */

public class EchoClient {


    private final String host;
    private final int port;

    public EchoClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start() throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            //构建Bootstrap 实例，得到客户端引导对象
            Bootstrap b = new Bootstrap();
            b.group(group)
                    //创建NioEventLoopGroup实例，处理不同事件，如建立新连接，读写数据等.
                    .channel(NioSocketChannel.class)
                    //初始化服务端InetSocketAddress.
                    .remoteAddress(new InetSocketAddress(host, port))
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            //创建一个handler，它会在连接建立的时候被执行
                            ch.pipeline().addLast(new EchoClientHandler());
                        }
                    });
            //erverBootstrap.connect()方法和服务端建立连接
            ChannelFuture f = b.connect().sync();
            f.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }
    public static void main(String[] args) throws Exception {
        final String host = "localhost";
        final int port = 53120;
        //和服务端建立连接
        new EchoClient(host, port).start();
    }
}
