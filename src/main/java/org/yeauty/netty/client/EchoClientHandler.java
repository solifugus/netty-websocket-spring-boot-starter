package org.yeauty.netty.client;

/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty.client
 * @Date 2024/2/24 3:01
 */

import io.netty.buffer.*;
import io.netty.channel.*;
import io.netty.util.CharsetUtil;

import java.util.Optional;

public class EchoClientHandler extends SimpleChannelInboundHandler<ByteBuf> {


    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        System.out.println("写入数据");
        //写入数据，发送给服务端
        ctx.writeAndFlush(Unpooled.copiedBuffer("hzw", CharsetUtil.UTF_8));
    }


    @Override
    public void channelRead0(ChannelHandlerContext ctx, ByteBuf in) {
        ByteBuf byteBuf = in.readBytes(in.readableBytes());
        byte[] bytes = nettyBuf2bytes(byteBuf);
        String str = new String(bytes);
        //等待从服务端接收数据
        System.out.println("客户端接收" + str);
    }


    public static byte[] nettyBuf2bytes(ByteBuf bbfMessage) {
        boolean bHasArray = bbfMessage.hasArray();

        byte[] abyRegisterMsg;
        if (bHasArray) {
            abyRegisterMsg = bbfMessage.array();
        } else {
            int iRecvBufSize = bbfMessage.readableBytes();

            abyRegisterMsg = new byte[iRecvBufSize];
            bbfMessage.getBytes(0, abyRegisterMsg);
        }

        return abyRegisterMsg;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        System.out.println("关闭连接");
        //关闭连接
        cause.printStackTrace();
        ctx.close();
    }
}
