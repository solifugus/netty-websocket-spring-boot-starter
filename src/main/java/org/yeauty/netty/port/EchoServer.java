package org.yeauty.netty.port;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty.port
 * @Date 2024/2/24 2:28
 */
public class EchoServer {

    private final int port;

    public EchoServer(int port) {
        this.port = port;
    }

    public void start() throws Exception {
        EventLoopGroup group = new NioEventLoopGroup(4);
        EventLoopGroup workGroup = new NioEventLoopGroup(20);
        //创建ServerBootstrap实例来引导绑定和启动服务器
        ServerBootstrap b = new ServerBootstrap();
        b.group(group, workGroup)
                //创建NioEventLoopGroup对象来处理事件，如接受新连接、接收数据、写数据等等
                .channel(NioServerSocketChannel.class)
                //指定InetSocketAddress，服务器监听此端口
                .localAddress(new InetSocketAddress(port))
                //设置队列大小
                .option(ChannelOption.SO_BACKLOG, 1024)
                // 两小时内没有数据的通信时,TCP会自动发送一个活动探测数据报文
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                //设置childHandler执行所有的连接请求
                .childHandler(new ChannelInitializer<Channel>() {
                    @Override
                    public void initChannel(Channel ch) throws Exception {
                        //把业务处理类（EchoServerHandler）放到容器中去
                        ch.pipeline().addLast(new EchoServerHandler());
                    }
                });
        try {
            //都设置完毕了，最后调用ServerBootstrap.bind()方法来绑定服务器
            //bind服务端，此时发生阻塞，直到bind调用完成，才会调用到sync()方法。
            //会一直等到服务端channel关闭的时候才会执行（因为我们在channel关闭的future上执行了sync()方法）
            ChannelFuture f = b.bind().sync();
            System.out.println(EchoServer.class.getName() + "开始监听：" + f.channel().localAddress());
            f.channel().closeFuture().sync();
        }catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }finally {
            group.shutdownGracefully().sync();
        }
    }

    public static void main(String[] args) throws Exception {
        new EchoServer(53120).start();
    }
}
