package org.yeauty.netty.port;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.concurrent.EventExecutorGroup;
import org.yeauty.util.NettyUtil;

/**
 * @author huangzhiwei
 * @Classname org.yeauty.netty.port
 * @Date 2024/2/24 2:41
 */
public class EchoServerHandler extends SimpleChannelInboundHandler<ByteBuf> {

//    @Override
//    public void channelRead(ChannelHandlerContext ctx, ByteBuf msg) {
//        System.out.println("消息" + msg);
//        ctx.write(msg);
//    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        byte[] msgByte = NettyUtil.nettyBuf2bytes(msg);
        System.out.println("消息" + new String(msgByte));
        System.out.println(msg.refCnt());

        ByteBuf bbPacket = Unpooled.copiedBuffer(msgByte);
        ctx.writeAndFlush(bbPacket);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER)
                .addListener(ChannelFutureListener.CLOSE);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
                                Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

}
